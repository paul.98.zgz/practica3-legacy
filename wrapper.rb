#!/usr/bin/env ruby
# Wrapper para una base de datos de programas de ZX Spectrum, escrita en BASIC
#
# Autores:
#   - Paul Hodgetts
#   - Ignacio Palacios García
#   - David Ubide Alaiz

require 'fileutils'
require 'timeout'

module Wrapper

  # Ruta del fichero temporal que se emplea para manipular el output de DATABASE
  FILE_OUTPUT      = "/tmp/prg.out"

  # Nombre del programa que se ejecuta en +pcbasic+
  BASIC_PRG        = "database"

  # Estructura para un registro en la base de datos
  Registro = Struct.new(:id, :name, :type, :tape)

  # Inicializa el programa en BASIC en escucha y ejecuta un script de interacción
  # con el programa DATABASE
  #
  # ===== Params:
  # +script+:: Nombre o ruta del script de acciones para el programa DATABASE
  def init_prg(script)
    FileUtils.touch FILE_OUTPUT unless File.exists? FILE_OUTPUT
    File.truncate(FILE_OUTPUT, 0) 

    `pcbasic -b #{BASIC_PRG} -q --output=#{FILE_OUTPUT} --input=#{script} 2> /dev/null`
  end

  # Devuelve el número de registros en la DATABASE
  # NOTA: El programa debe encontrarse en el menú principal
  def get_total
    init_prg "get_total"

    _, total, _ = `head -n11 #{FILE_OUTPUT} | tail -n1`.split(" ", -1).reject(&:empty?)
    return total
  end

  # Devuelve la información de un programa dado su nombre. Si no existe, 
  # devuelve +nil+.
  #
  # ===== Params:
  # +nombre+:: Nombre del programa a buscar
  def get_datos_programa(nombre)
    reg = Registro.new(nil, nil, nil, nil)

    text = File.read("get_datos_programa.template")
    new_contents = text.gsub(/--nombre--/, nombre)
    File.open("get_datos_programa", "w") do |file| file.puts new_contents end

    init_prg("get_datos_programa")
    datos = `head -n13 #{FILE_OUTPUT} | tail -n1`

    if datos == "NO HAY NINGUN PROGRAMA CON ESE NOMBRE; PULSA ENTER\n"
      reg = nil
    else
      reg_list = datos.split(" ", -1).reject(&:empty?)
    
      reg.id = reg_list[0]
      reg.tape = reg_list[-1].delete_prefix "CINTA:"
      reg.type = reg_list[-2]

      name_limit = reg_list.length - 3
      reg.name = ""

      if reg.type == "MESA"
        reg.type = "JUEGO DE MESA"
        name_limit -= 2
      elsif reg.type == "DEPORTIVO"
        reg.type = "S. DEPORTIVO"
        name_limit -= 1
      elsif reg.type == "---"
        reg.type = "SIN CATEGORIA"
      end
      (2..name_limit).each do |i|
        reg.name += reg_list[i] + " "
      end      

      reg.name.delete_suffix(' ')

      puts reg.id
      puts reg.name
      puts reg.type
      puts reg.tape
    end
    return reg
  end

  # Devuelve todos los programas dado el nombre de una cinta. En caso de que
  # no haya ningún programa con tal cinta, o ésta no exista, devuelve una
  # lista vacía.
  #
  # ===== Params:
  # +cinta+:: Nombre de la cinta a recuperar
  def get_datos_cinta(cinta)
    regs = []

    init_prg("get_datos_cinta")

    raw_data = `head -n-14 #{FILE_OUTPUT} | tail -n+15 | sed "s/^PULS.*\xA7.*REGISTRO//g" | sed "s/^ //g"`
    all_data = raw_data.lines

    ids = []
    (0...all_data.length).step(5).each do |index|
      if all_data[index][0..-3].length < 4
        ids.append(all_data[index][0..-3])
      else
        ids.append(all_data[index][0..-3][-10..-1].split(" ").last)
      end
    end

    names = []
    (1...all_data.length).step(5).each do |index|
      names.append(all_data[index][0..-3])
    end

    types = []
    (2...all_data.length).step(5).each do |index|
      if all_data[index][0..-3] == "---"
        types.append("SIN CATEGORIA")
      else
        types.append(all_data[index][0..-3])
      end
    end

    tapes = []
    (3...all_data.length).step(5).each do |index|
      tapes.append(all_data[index][0..-3])
    end

    indexes = tapes.each_index.select do |index|
      (tapes[index] =~ /#{cinta}/ ||
      tapes[index] =~ /.*-#{cinta}/ ||
      tapes[index] =~ /#{cinta}-.*/)
    end

    for i in indexes
      regs.append Registro.new(ids[i],names[i],types[i],tapes[i])
    end

    return regs
  end
end